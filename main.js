import Expo from 'expo';
import React from 'react';
import { 
    Button,
    ScrollView,
    StyleSheet,
    Text,
    TextInput,
    View,
    StatusBar } from 'react-native';

import List from './List';

class App extends React.Component {
  constructor(props) {
        super(props)
        
        //deklarasi state
        this.state = {
            text: '',
            todos: []
        }
    } 

    //tambah todo
    add = () => {
        //Dapatkan value dari state sekarang
        const { text, todos } = this.state

        //jika text kosong, skip
        if (!text) return;

        //Masukkan todo baru ke library
        todos.push(text)

        //set State baru
        this.setState({
            todos: todos,
            text: ''
        })
    }

    // Hapus todo
    remove = (todo) => {
        const { todos } = this.state

        //cari index dari todo
        todoWillDelete = todos.indexOf(todo)

        // hapus todo
        todos.splice(todoWillDelete, 1)

        // set state baru
        this.setState({
            todos: todos
        })
    }

  render() {
    return (
      <ScrollView>
                <View style= {styles.container}>
                    <StatusBar
                        backgroundColor="blue"
                        barStyle="light-content" />
                    <View style = {styles.spacer} />
                    <Text>Simple Todo Apps</Text>

                    <View style = {styles.spacer} />

                    {/*Form input TODO*/}
                    <View>
                        <TextInput 
                        style = {styles.input}
                        onChangeText={(text) => this.setState({text})}
                        value = {this.state.text} />

                        <Button
                        style={styles.buttonContainer}
                        title="Simpan"
                        onPress={this.add} />
                    </View>

                    <View style = {styles.spacer} />

                    {/*Tampilan TODOs jika ada data*/}
                    {this.state.todos.length > 0 &&
                        <List items = {this.state.todos} onRemove={this.remove} />
                    }
                </View>
            </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20
    },
    button:{},
    textInput: {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        padding: 10
    },
    spacer: {
        left: 0,
        right: 0,
        height: 30
    },
    input: {
		height: 50,
    fontSize: 18,
		backgroundColor: 'rgba(255,255,255,0.2)',
		marginBottom: 10,
		paddingHorizontal: 10
	},
	buttonContainer: {
		backgroundColor: '#2980b9',
		paddingVertical: 15
	}
})

Expo.registerRootComponent(App);
